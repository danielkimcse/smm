/*
This view represents the display of the entire app
*/
var ListContainerView = SOCIView.extend({
  template: _.template($('#ListContainerView').text()),
  collection: new ListItemCollection(),
  className: 'ListContainerView',
  limit: 5,
  currentPage: 1,
  sortedBy: 'default',
  events: {
    "keyup #search": "search",
    "click .searchRadio": "search",
    "click #next": "next",
    "click #prev": "prev",
    "click #bySchedule": function() { this.sort('schedule'); },
    "click #byCreatedDate": function() { this.sort('created_at'); },
    "click #byCreator": function() { this.sort('created_by_name'); }
  },
  //read input and display items that starts with the input
  search: function(e) {
    var self = this;
    this.input = this.$("#search");
    this.$("#item-list tr").slice(1).remove(); //empty except table header
    var searchRadio = $('input[name=searchRadio]');
    var checked = searchRadio.filter(':checked').val();
    var count = this.limit;
    //iterate through each models
    _.each(this.collection.models, function(model) {
      //if # of items hits the limit, skip
      if (count) {
        //display all elements if input is empty
        if (self.input.val() === '') {
          var liv = new ListItemView({ model: model });
          self.$el.find("#item-list").append(liv.render().$el);
          count--;
        }
        //search by message
        else if (checked === "Search by message") {
          //if msg starts with input
          var msgStarts = model.toJSON().message.toLowerCase().startsWith(self.input.val().toLowerCase());
          if (msgStarts) {
            var liv = new ListItemView({ model: model });
            self.$el.find("#item-list").append(liv.render().$el);
            count--;
          }
        }
        //search by name
        else if (checked === "Search by name") {
          //if msg starts with input
          var msgStarts = model.toJSON().created_by_name.toLowerCase().startsWith(self.input.val().toLowerCase());
          //if msg starts with input
          if (msgStarts) {
            var liv = new ListItemView({ model: model });
            self.$el.find("#item-list").append(liv.render().$el);
            count--;
          }
        }
      }
    });
  },
  //go to next page
  next: function() {
    this.currentPage++;
    this.render();
  },
  //go to previous page
  prev: function() {
    this.currentPage--;
    this.render();
  },
  //sort the items
  sort: function(sortBy) {
    //if already clicked, reverse the order
    if (this.sortedBy === sortBy) {
      this.collection.set(this.collection.models.reverse(), { sort: false });
      this.render();
      this.sortedBy = 'default';
      this.$el.find(`#${sortBy}`).css("transform", "rotate(180deg)");
    }
    //if not clicked, sort normally
    else {
      this.collection.comparator = sortBy;
      this.collection.reset(this.collection.models);
      this.render();
      this.sortedBy = sortBy;
    }
  },
  initialize: function() {
    //render again when a model is removed from this collection
    this.listenTo(this.collection, 'remove', this.render);
  },
  //disable next and prev button when items don't exist
  //display # of items under the limit
  render: function() {
    var self = this;
    this.$el.html(this.template(this.collection));

    var start = (this.currentPage - 1) * this.limit; //start index
    //if next page contains no items
    if (!this.collection.models[start + this.limit]) {
      var nextEl = this.$el.find("#next");
      nextEl.prop("disabled", true);
      nextEl.css("background-color", "#A8A8A8");
      nextEl.css("color", "white");
    }
    //if on first page
    if (this.currentPage === 1) {
      var prevEl = this.$el.find("#prev");
      prevEl.prop("disabled", true);
      prevEl.css("background-color", "#A8A8A8");
      prevEl.css("color", "white");
    }
    //create item views for current page
    _.each(this.collection.models.slice(start, start + self.limit), function(model) {
      var liv = new ListItemView({ model: model });
      self.$el.find("#item-list").append(liv.render().$el);
    });
    //update collection attributes
    this.collection.recalc();
    return this;
  }
});