/*
This view represents the display each items
*/
var ListItemView = SOCIView.extend({

  tagName: "tr",
  model: new ListItemModel(),
  template: _.template($('#ListItemView').text()),
  events: {
    "click td:not(:last-child)": "expand",
    "click .delete": "clear"
  },
  //open dialog
  expand: function() {
    var mv = new ModalView({ model: this.model });
    mv.render().$el.prependTo("body");
  },
  //remove this model from collection and this view
  clear: function() {
    this.model.collection.remove(this.model);
    this.remove();
  },
  //set template
  initialize: function() {
    this.template = _.template($('#ListItemView').html());
  },

  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    //change color for status
    this.$('.status:contains("Approved")').css("color", "#4C9C00");
    this.$('.status:contains("Pending")').css("color", "#555555");
    this.$('.status:contains("Rejected")').css("color", "#E00000");
    return this;
  }
});