/*
This view represents the small dialog box when opening a post
*/
var ModalView = SOCIView.extend({

  tagName: "div",
  template: _.template($('#ModalView').text()),
  model: new ListItemModel(),
  className: "dialog",
  events: {
    "click .close": "close",
  },

  //close dialog
  close: function() {
    this.remove();
  },
  initialize: function() {
    this.template = _.template($('#ModalView').html());
  },
  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    //change color for status
    this.$('.status:contains("Approved")').css("color", "#4C9C00");
    this.$('.status:contains("Pending")').css("color", "#555555");
    this.$('.status:contains("Rejected")').css("color", "#E00000");
    return this;
  }
});